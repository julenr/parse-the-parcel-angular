# Parse-the-Parcel Project
The goal of this project is to implement a component that, when supplied the dimensions (length x breadth x height) and weight of a package, can advise on the cost and type of package required. If the package exceeds these dimensions - or is over 25kg - then the service should not return a packaging solution.

## Guidelines
  - You will be expected to produce a solution that solves the above problem; while this is a simple component we will be executing it in a production system so it will need to be production ready and supportable. We also expect that as our shipping capacity grows so too will our service offering - meaning that one day we might support Extra Large Packages, or Overweight services.
  - You are free to choose the technologies you think are either best for the system or best show your abilities. You are welcome to make assumptions about the solution along with any improvements you think enhance or add value to the solution - though please be aware of the original scope.
  - You should spend about an hour or two on this task

## Technology Characteristics
Create a component that:
  - Uses ES6 transpiled with [Babel](https://babeljs.io/).
  - Webpack is used to bundle the App.
  - Organize and coordinate the UI state using Angular components strategy.
  - SASS-BEM styling approach to keep styles modular.
  - For unit testing the technologies are Karma, mocha, chai and Enzyme to test components.

## Quick start
  1. Run `npm install` to install dependencies.
  2. Run `npm start` to run the App locally through Webpack-Dev-Server.
  3. Run `npm run build` to start Webpack and bundle the App.
  4. Run `npm test` to run the unit test process.
  5. Run `npm run tdd` to keep karma running.

  Now you're ready to rumble!

## Design Characteristics
  - Angular 1.5 component.
  - Easy to extend the number of steps and add new variables to calculate the result. (support Extra Large Packages, or Overweight services or new attributes like days to deliver)
  - Tail call recursive implementation.

 **Julen Rojo** rojo.julen@gmail.com
