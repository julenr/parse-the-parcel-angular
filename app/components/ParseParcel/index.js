import angular from 'angular';

import controller from './ParseParcel.controller';
import template from './ParseParcel.template.html';

import './ParseParcel.style.scss';

export default angular.module('parseparcel', [])
  .component('parseParcel', {
    template,
    controller,
    bindings: {
      dimensions: '<dimensions',
      rotatable: '<'
    }
  })
.name;
