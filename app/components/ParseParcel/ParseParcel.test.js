describe('Component: ParseParcel', function () {
  let element;
  let scope;

  beforeEach(angular.mock.module('main'));

  /*@ngInject*/
  beforeEach(inject(function($rootScope, $compile){
    scope = $rootScope.$new();
    element = angular.element('<parse-parcel dimensions="dimensions" rotatable="rotatable" />');
    element = $compile(element)(scope);
  }));

  it('should render the value $5.00', function() {
    scope.dimensions = {
        length: 100,
        breadth: 100,
        height: 100,
        weight: 20
    };
    scope.rotatable = false;
    scope.$apply();
    const result = element.find('span');
    expect(result.text()).to.match(/\$5.00/g);
  });

  it('should render the value $7.50', function() {
    scope.dimensions = {
        length: 220,
        breadth: 100,
        height: 100,
        weight: 20
    };
    scope.rotatable = false;
    scope.$apply();
    const result = element.find('span');
    expect(result.text()).to.match(/\$7.50/g);
  });

  it('should render the value $8.50', function() {
    scope.dimensions = {
        length: 300,
        breadth: 100,
        height: 100,
        weight: 20
    };
    scope.rotatable = false;
    scope.$apply();
    const result = element.find('span');
    expect(result.text()).to.match(/\$8.50/g);
  });

});
