// Calculate permutations
function swap(array, pos1, pos2) {
    const temp = array[pos1];
    array[pos1] = array[pos2];
    array[pos2] = temp;
  }

function calcPermute(array, result = [], num) {
  const n = num || array.length;
  if (n === 1) {
    result.push([...array]);
  } else {
    for (var i = 1, j = 0; i <= n; i += 1) {
      calcPermute(array, result, n - 1);
      if (n % 2) {
        j = 1;
      } else {
        j = i;
      }
      swap(array, j - 1, n - 1);
    }
  }
}

function permute(collection) {
  const result = [];
  calcPermute(collection, result);
  return result;
}

export default class ParseParcelController {
  /*@ngInject*/
  constructor($scope) {
    this.scope = $scope;
    $scope.computeResult = (dims, rotate = false) => this.computeResult(dims, rotate);
    this.steps = 3;
    this.calcRules = {
      length: [210, 280, 380],
      breadth: [280, 390, 550],
      height: [130, 180, 200],
      weight: [25, 25, 25],
      values: ['5.00', '7.50', '8.50', null]
    };
    this.permute = permute;
  }

  computeResult(dims, rotate = false) {
    let value = this.steps;
    if (rotate) {
      for (let comb of this.permute([dims.length, dims.breadth, dims.height])) {
        let obj = Object.assign({}, dims, {length: comb[0], breadth: comb[1], height: comb[2]});
        let temp = this.calcStep(obj, this.steps - 1);
        value = (temp < value) ? temp : value;
        if (value === 0) {
          break;
        }
      }
    } else {
      value = this.calcStep(dims, this.steps - 1);
    }
    return ((value < this.steps) ? `\$${this.calcRules.values[value]}` : '');
  }

  calcStep(dims, step = 0) {
    for (let dim in dims) {
      if (dims[dim] > this.calcRules[dim][step]) {
        return step + 1;
      }
    }
    if (this.isFinalStep(step)) return 0;
    return this.calcStep(dims, step - 1);
  }

  isFinalStep(step) {
    return step === 0;
  }
}
