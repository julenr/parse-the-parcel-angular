import angular from 'angular';
import uirouter from 'angular-ui-router';

import routing from './app.routes';
import AppController from './app.controller';
import './app.style.scss';

export default angular.module('App',
    [ uirouter ])
    .config(routing)
    .controller('AppController', AppController)
    .name;
