
import template from './app.template.html';

export default function routes($stateProvider) {
    'ngInject';
    $stateProvider
        .state('app', {
            template,
            url: '/app',
            controller: 'AppController',
            controllerAs: 'app'
        });
}
