export default class AppController {
  /*@ngInject*/
  constructor($scope) {
    $scope.title = {name: 'Parse the parcel - Angular 1.5 component driven version'};
    $scope.dimensions = {
        length: 0,
        breadth: 0,
        height: 0,
        weight: 0
    };
    $scope.rotatable = false;
  }
}
