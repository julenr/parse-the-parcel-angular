import 'babel-polyfill';

import angular from 'angular';
import uirouter from 'angular-ui-router';

import routing from './index.config';

// Components
import App from './components/App';
import ParceParcel from './components/ParseParcel';

angular
  .element(document)
  .ready(function () {
    const appName = 'main';
    const body = document.getElementsByTagName('body')[0];
    const appAngular = angular.module(appName,
      [
        uirouter,
        App,
        ParceParcel
      ])
      .config(routing);
    angular.bootstrap(body, [appAngular.name], {strictDi: true});
  });
