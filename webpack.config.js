//
//
//              Created by
//            __ _____ __    _____ _____    _____ _____    __ _____
//         __|  |  |  |  |  |   __|   | |  | __  |     |__|  |     |
//        |  |  |  |  |  |__|   __| | | |  |    -|  |  |  |  |  |  |
//        |_____|_____|_____|_____|_|___|  |__|__|_____|_____|_____|
//
//                on 12/09/2016
//                   isusk246@gmail.com
//
//


const path = require('path');
const HtmlwebpackPlugin = require('html-webpack-plugin');
const merge = require('webpack-merge');
const webpack = require('webpack');
const Clean = require('clean-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const autoprefixer = require('autoprefixer');
const precss = require('precss');
const VersionFile = require('webpack-version-file-plugin');

const pkg = require('./package.json');

const TARGET = process.env.npm_lifecycle_event;
const PATHS = {
  app: path.join(__dirname, 'app'),
  build: path.join(__dirname, 'build'),
  develop: path.join(__dirname, 'build-develop'),
  test: path.join(__dirname, 'app')
};
const APP_TITLE = 'SelfService';

process.env.BABEL_ENV = TARGET;

const common = {
  entry: PATHS.app,
  resolve: {
    extensions: ['', '.js']
  },
  output: {
    path: PATHS.build,
    filename: '[name].js'
  },
  module: {
    loaders: [
      {
        test: /\.js$/,
        loaders: ['ng-annotate', 'babel?cacheDirectory'],
        include: PATHS.app
      },
      {
        test: /\.(png|jpg|gif|svg)$/,
        loaders: ['url-loader?limit=70000&name=assets/images/[name].[ext]'],
        include: PATHS.imagesAssets
      },
      {
        test: /\.(ttf|eot)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        loader: 'file-loader?name=assets/fonts/[name].[ext]'
      },
      {
        test: /\.(woff|woff2)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        loader: 'url-loader?limit=70000&name=assets/fonts/[name].[ext]'
      },
      {
        test: /\.json$/,
        loader: 'json'
      },
      { test: /\.html$/, loader: 'html', include: PATHS.app }
    ]
  },
  plugins: [
    new webpack.DefinePlugin({
      '__VERSION__': JSON.stringify(pkg.version)
    })
  ]
};

if (TARGET === 'start' || TARGET === 'start:mocked' || !TARGET) {
  module.exports = merge(common, {
    devtool: '#eval-source-map',
    devServer: {
      historyApiFallback: true,
      inline: true,
      progress: true,
      watchOptions: {
        poll: 1000
      },

      // display only errors to reduce the amount of output
      stats: 'errors-only',

      // parse host and port from env so this is easy
      // to customize
      host: '0.0.0.0',
      port: 8085
    },
    module: {
      loaders: [
        // Define development specific CSS setup
        { test: /\.css$/, loader: 'style!css!postcss'},
        { test: /\.scss$/, loader: 'style!css!postcss!sass', include: PATHS.app }
      ]
    },
    postcss: [ precss, autoprefixer({ browsers: ['last 4 versions'] }) ],
    plugins: [
      new webpack.HotModuleReplacementPlugin(),
      new HtmlwebpackPlugin({
        template: './templates/index.webpack.ejs',
        title: APP_TITLE,
        unsupportedBrowser: true,
        mobile: true,
        window: {
          env: {
            apiHost: ''
          }
        }
      }),
      new webpack.DefinePlugin({
        '__DEV__': JSON.stringify(JSON.parse('true')),
        '__MOCKED_BACKEND__': JSON.stringify(JSON.parse( (TARGET === 'start:mocked') ? 'true' : 'false'))
      })
    ],
    preLoaders: [
      {
        test: /\.js?$/,
        loaders: ['eslint'],
        include: PATHS.app
      }
    ]
  });
}

if (TARGET === 'build' || TARGET === 'build:mocked' || TARGET === 'release') {
  const vendorPkgs = Object.keys(pkg.dependencies);
  vendorPkgs.splice(vendorPkgs.indexOf('acc-theme'), 1);
  module.exports = merge(common, {
    // Define entry points needed for splitting
    entry: {
      app: PATHS.app,
      vendor: vendorPkgs
    },
    output: {
      path: PATHS.build,
      filename: '[name].[chunkhash].js',
      chunkFilename: '[chunkhash].js'
    },

    module: {
      loaders: [
        { test: /\.scss$/, loader: ExtractTextPlugin.extract('style', 'css!postcss!sass'), include: PATHS.app },
        { test: /\.css$/, loader: ExtractTextPlugin.extract('style', 'css!postcss')}
      ]
    },

    postcss: [ precss, autoprefixer({ browsers: ['last 4 versions'] }) ],
    plugins: [
       new Clean([PATHS.build], {
        verbose: true,
        dry: false
      }),
      // Output extracted CSS to a file
      new ExtractTextPlugin('styles.[chunkhash].css'),
      // Extract vendor and manifest files
      new webpack.optimize.CommonsChunkPlugin({
        names: ['vendor', 'manifest']
      }),
     new VersionFile({
          packageFile: path.join(__dirname, 'package.json'),
          template: './templates/version.ejs',
          outputFile: 'version.txt'
      }),
      new webpack.DefinePlugin({
        'process.env': {
          'NODE_ENV': JSON.stringify('production')
        },
        '__DEV__': JSON.stringify(JSON.parse('false')),
        '__MOCKED_BACKEND__': JSON.stringify(JSON.parse( (TARGET === 'build:mocked') ? 'true' : 'false'))
      }),
      new HtmlwebpackPlugin({
        title: APP_TITLE,
        template: './templates/index.production.ejs',
        unsupportedBrowser: true,
        mobile: true,
        baseHref: '/business/self-service/',
        window: {
          env: {
            apiHost: ''
          }
        }
      }),

      new webpack.optimize.UglifyJsPlugin({
        compress: {
          warnings: false
        }
      }),
      new CopyWebpackPlugin([
        { from: PATHS.app + '/assets/fonts', to: './assets/fonts' },
        { from: PATHS.app + '/assets/images', to: './assets/images' },
        { from: PATHS.app + '/assets/js', to: './assets/js' },
        { from: __dirname + '/version.txt', to: './version', toType: 'file' },
        { from: __dirname + '/app/assets/js/eluminate.js', to: 'eluminate.js', toType: 'file' }
      ])
    ]
  });
}

if (TARGET === 'develop') {
  module.exports = merge(common, {
    entry: {
      app: PATHS.app,
      vendor: Object.keys(pkg.dependencies)
    },
    output: {
      path: PATHS.develop,
      filename: '[name].[chunkhash].js',
      chunkFilename: '[chunkhash].js'
    },
    devtool: '#eval-source-map',
    module: {
      loaders: [
        { test: /\.scss$/, loader: ExtractTextPlugin.extract('style', 'css!postcss!sass'), include: PATHS.app },
        { test: /\.css$/, loader: ExtractTextPlugin.extract('style', 'css!postcss') }
      ]
    },
    postcss: [ precss, autoprefixer({ browsers: ['last 4 versions'] }) ],
    plugins: [
      new Clean([PATHS.develop], {
        verbose: true,
        dry: false
      }),
      // Output extracted CSS to a file
      new ExtractTextPlugin('styles.[chunkhash].css'),
      // Extract vendor and manifest files
      new webpack.optimize.CommonsChunkPlugin({
        names: ['vendor', 'manifest']
      }),
      // Setting DefinePlugin affects React library size!
      new webpack.DefinePlugin({
        'process.env': {
          'NODE_ENV': JSON.stringify('production')
        },
        '__DEV__': JSON.stringify(JSON.parse('false')),
        '__BOOTSTRAP__': JSON.stringify(JSON.parse('true'))
      }),
      new HtmlwebpackPlugin({
        title: APP_TITLE,
        template: './templates/index.develop.ejs',
        unsupportedBrowser: true,
        mobile: true,
        window: {
          env: {
            apiHost: ''
          }
        }
      }),
      new webpack.optimize.UglifyJsPlugin({
        compress: {
          warnings: false
        }
      }),
      new CopyWebpackPlugin([
        { from: PATHS.app + '/assets', to: './assets/' }
      ])
    ]
  });
}

if (TARGET === 'test' || TARGET === 'tdd') {
  module.exports = merge(common, {
    entry: {}, // karma will set this
    output: {}, // karma will set this
    devtool: 'inline-source-map',
    resolve: {
      alias: {
        'src': PATHS.app
      }
    },
    module: {
      preLoaders: [
        {
          test: /\.js?$/,
          loaders: ['isparta-instrumenter'],
          include: PATHS.app
        }
      ],
      loaders: [
        { test: /\.scss$/, loader: ExtractTextPlugin.extract('style', 'css!postcss!sass'), include: PATHS.app },
        { test: /\.css$/, loader: ExtractTextPlugin.extract('style', 'css!postcss') }
      ]
    },
    postcss: [ precss, autoprefixer({ browsers: ['last 4 versions'] }) ],
    plugins: [
      new webpack.DefinePlugin({
        '__DEV__': JSON.stringify(JSON.parse('false'))
      }),
      new ExtractTextPlugin('styles.[chunkhash].css')
    ]
  });
}
